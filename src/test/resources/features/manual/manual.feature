Feature: example of all manual testing features

  @manual:passed
  @manual-last-tested:sprint-1
  @manual-test-evidence:assets/testing_proof.jpeg
  Scenario: manual testing scenario
    Given I go to google
    When I search for "something" manually
    Then I found the result