package serenity.bdd.framework.sample.pageObjects.google;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.support.FindBy;


@DefaultUrl("page:google.homePage")
public class GooglePage extends PageObject {
    @FindBy(name = "q")
    private WebElementFacade searchBox;

    @FindBy(xpath = "//div[text()='Accept all']")
    private WebElementFacade acceptCookiesBtn;


    public void acceptCookies() {
        acceptCookiesBtn.click();
    }

    public void search(String searchTerm) {
        searchBox.typeAndEnter(searchTerm);
    }
}
