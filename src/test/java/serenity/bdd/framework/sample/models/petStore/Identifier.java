package serenity.bdd.framework.sample.models.petStore;

import serenity.bdd.framework.sample.utils.generators.PositiveInteger;
import lombok.Data;
import org.jeasy.random.annotation.Randomizer;

@Data
public class Identifier {
    @Randomizer(PositiveInteger.class)
    private Integer id;
    private String name;
}
