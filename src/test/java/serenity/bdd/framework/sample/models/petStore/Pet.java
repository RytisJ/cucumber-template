package serenity.bdd.framework.sample.models.petStore;

import serenity.bdd.framework.sample.utils.generators.PositiveInteger;
import lombok.Data;
import org.jeasy.random.annotation.Randomizer;

import java.util.List;

@Data
public class Pet {

    @Randomizer(PositiveInteger.class)
    private Integer id;
    private Identifier category;
    private String name;
    private List<String> photoUrls;
    private List<Identifier> tags;
    private String status;
}
