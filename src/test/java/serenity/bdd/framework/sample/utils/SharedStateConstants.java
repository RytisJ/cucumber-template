package serenity.bdd.framework.sample.utils;

public enum SharedStateConstants {
    CONFIG,
    PET,
    API_RESPONSE,
    SEARCH_TERM
}
