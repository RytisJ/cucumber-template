package serenity.bdd.framework.sample.utils;

import io.cucumber.java.Before;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;

import static serenity.bdd.framework.sample.utils.SharedStateConstants.CONFIG;

public class Hooks {

    private EnvironmentVariables environmentVariables;

    /**
     * save config so it can be accessed outside step files
     */
    @Before
    public void setUpConfig() {
        Serenity.setSessionVariable(CONFIG).to(EnvironmentSpecificConfiguration.from(environmentVariables));
    }
}
