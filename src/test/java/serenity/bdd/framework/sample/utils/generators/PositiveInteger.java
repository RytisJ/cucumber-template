package serenity.bdd.framework.sample.utils.generators;

import org.jeasy.random.api.Randomizer;

import java.util.Random;

public class PositiveInteger implements Randomizer<Integer> {
    @Override
    public Integer getRandomValue() {
        return new Random().nextInt(1000);
    }
}
