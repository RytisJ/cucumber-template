package serenity.bdd.framework.sample.steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import serenity.bdd.framework.sample.pageObjects.google.GooglePage;
import serenity.bdd.framework.sample.utils.SharedStateConstants;

import static org.assertj.core.api.Assertions.assertThat;

public class GoogleSearchSteps {

    private GooglePage googlePage;

    @Given("I on google search")
    public void iOnGoogleSearch() {
        googlePage.open();
        googlePage.acceptCookies();
    }

    @When("I search for word {string}")
    public void iSearchForWord(String searchTerm) {
        googlePage.search(searchTerm);
        Serenity.setSessionVariable(SharedStateConstants.SEARCH_TERM).to(searchTerm);
    }

    @Then("I found information about my search term")
    public void iFoundInformationAboutMySearchTerm() {
        String searchTerm = Serenity.sessionVariableCalled(SharedStateConstants.SEARCH_TERM);
        assertThat(googlePage.getTitle()).isEqualTo(searchTerm + " - Google Search");
    }
}
