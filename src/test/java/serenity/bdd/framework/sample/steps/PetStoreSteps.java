package serenity.bdd.framework.sample.steps;

import serenity.bdd.framework.sample.connectors.PetEndpointConnector;
import serenity.bdd.framework.sample.models.petStore.Pet;
import serenity.bdd.framework.sample.utils.Helpers;
import serenity.bdd.framework.sample.utils.SharedStateConstants;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;

import static org.assertj.core.api.Assertions.assertThat;

public class PetStoreSteps {
    private PetEndpointConnector petEndpointConnector = new PetEndpointConnector();

    @Given("I am on pet shop admin page")
    public void iAmOnPetShopAdminPage() {
        Pet pet = Helpers.easyRandom.nextObject(Pet.class);
        Serenity.setSessionVariable(SharedStateConstants.PET).to(pet);
    }

    @When("I try to add a pet")
    public void iTryToAddAPet() {
        Pet pet = Serenity.sessionVariableCalled(SharedStateConstants.PET);
        Serenity.setSessionVariable(SharedStateConstants.API_RESPONSE).to(petEndpointConnector.addPet(pet));
    }

    @Then("pet is added successfully")
    public void petIsAddedSuccessfully() {
        Pet apiResponse = Serenity.sessionVariableCalled(SharedStateConstants.API_RESPONSE);
        Pet expectedPet = Serenity.sessionVariableCalled(SharedStateConstants.PET);
        assertThat(apiResponse).isEqualTo(expectedPet);
    }
}
