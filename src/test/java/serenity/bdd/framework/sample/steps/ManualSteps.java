package serenity.bdd.framework.sample.steps;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ManualSteps {
    @Given("I go to google")
    public void iGoToGoogle() {
    }

    @When("I search for {string} manually")
    public void iSearchForManually(String arg0) {
    }

    @Then("I found the result")
    public void iFoundTheResult() {
    }
}
