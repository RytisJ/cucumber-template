package serenity.bdd.framework.sample;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty"},
        features = "src/test/resources/features",
        glue = {
                "serenity.bdd.framework.sample.steps",
                "serenity.bdd.framework.sample.utils",
                "serenity.bdd.framework.sample.pageObjects"
        }
)
public class CucumberTestSuite {
}
