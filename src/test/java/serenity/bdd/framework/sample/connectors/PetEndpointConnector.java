package serenity.bdd.framework.sample.connectors;

import io.restassured.specification.RequestSpecification;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import serenity.bdd.framework.sample.models.petStore.Pet;

import static net.serenitybdd.rest.SerenityRest.given;
import static serenity.bdd.framework.sample.utils.SharedStateConstants.CONFIG;

public class PetEndpointConnector {

    private final String baseUrl = ((EnvironmentSpecificConfiguration) Serenity.sessionVariableCalled(CONFIG))
            .getProperty("petStore.baseUrl");

    private final RequestSpecification connector = given()
            .baseUri(baseUrl + "/v2/pet")
            .header("Content-Type", "application/json");

    /**
     * adding a pet to the store
     * mostly for happy path testing
     *
     * @param pet
     * @return
     */
    public Pet addPet(Pet pet) {
        return connector.with()
                .body(pet)
                .post()
                .andReturn()
                .as(Pet.class);
    }
}
